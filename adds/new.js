app.controller('AddsNewController', ['$scope', '$firebase', '$firebaseAuth', function($scope, $firebase) {

  $scope.myRequired = function(element){
    return (element.$dirty && (element.$error.required || element.$invalid));
  };

  var ref = new Firebase("https://sf-adds.firebaseio.com/adds");
  $scope.addAdd = function(email, phone, title, body, faceBookId) {
  
    // Nullify undefined optional form fields.
    phone |= null;
    //faceBookId |= null;

    $scope.adds.$add({
      email: email,
      phone: phone,
      title: title,
      body: body,
      faceBookId: faceBookId,
    });
  };
}]);
