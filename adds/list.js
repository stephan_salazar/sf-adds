app.controller('AddsListController', ['$scope', '$firebase', '$firebaseAuth', '$filter', function($scope, $firebase, $filter) {
  var ref = new Firebase("https://sf-adds.firebaseio.com/adds");
  var sync = $firebase(ref);
  $scope.adds = sync.$asArray();
  $scope.bodyMaxShort=300;
}]);
