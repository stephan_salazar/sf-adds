var app = angular.module("addsApp", ["ngRoute", "firebase"]);

app.config(function($routeProvider){
  $routeProvider.when('/', {templateUrl:'default/index.html'});
  $routeProvider.when('/about', {templateUrl:'about/index.html'});
  $routeProvider.when('/add/:id', {
    templateUrl:'adds/details.html',
    controller: 'AddDetails'
    });

});



// Creating all the directives dinamically with a template patter.
// Meta programming is when code is used to write code.
// This block creates one Angular Directive for each element in directiveList.
// So that the code stays DRY.

function metaCreateDirectives(hashitis){

  var component = hashitis.component;
  var restrict = hashitis.restrict;
  if (restrict === undefined) restrict = 'E';
  var directiveList = hashitis.directiveList;

  directiveList.forEach(function(element) {
    app.directive(component + element.capitalize(), [ function() {
      return {
        restrict: restrict,
        templateUrl: component + '/' + element + '.html',
        controller: component.capitalize() + element.capitalize() + 'Controller'
      }
    }]);
  });
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

metaCreateDirectives({
  component: 'common',
  restrict: 'E',
  directiveList: ['header']
});

metaCreateDirectives({
  component: 'adds',
  restrict: 'E',
  directiveList: ['list', 'new']
});
