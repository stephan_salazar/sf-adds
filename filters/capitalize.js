app.filter('capitalize', function(){
  return function(input){
    if (input === undefined) return'';
    return input.charAt(0).toUpperCase() + input.substring(1);
  }
});
